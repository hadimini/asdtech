import os
import json
from _datetime import datetime as dt
from django.contrib.auth import login as auth_login, get_user_model
# from django.contrib.auth.forms import UserCreationForm
from .forms import UserCreationForm
from django.http import JsonResponse
from django.shortcuts import render, HttpResponseRedirect, HttpResponse, redirect

from .decorators import anonymous_required

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# Shop Items
data = [
        {
            "id": 1,
            "name": "Book",
            "color": "Red",
            "issue_date": "10/04/2018",
            "in_stock": True,
            "price": "20,5",
            "image": "https://i.pinimg.com/236x/98/a5/1f/98a51f3e27a962bf86951645d7a9bd9d--sales-quotes-product-launch.jpg"
        },
        {
            "id": 2,
            "name": "Card",
            "color": "Blue",
            "issue_date": "10/01/2019",
            "in_stock": False,
            "price": "50,5",
            "image": "https://t4.ftcdn.net/jpg/00/20/09/21/500_F_20092108_lm2cmPLAybmPHgHAlofQpCGzjawyoSdQ.jpg"
        },
        {
            "id": 3,
            "name": "Cat",
            "color": "Black",
            "issue_date": "12/30/2018",
            "in_stock": True,
            "price": "99,9",
            "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRY4qFB5p8hSQyqip37Lj0ZfR_YcQr-sITkHW9ipxcqznV0mH6Uqg"
        },

    ]

def addToCart(request):
    if request.is_ajax():
        id = request.POST.get('id')

        cartItemsIds = request.session['cartItemsIds']

        item = filter(lambda item: item['id'] == int(id), data) # fetch item from data ^^

        cartItemsSession = request.session['cartItems']
        cartItemsSession.append(list(item)[0])
        request.session['cartItems'] = cartItemsSession

        if id not in cartItemsIds:
            cartItemsIds.append(id)
        request.session['cartItemsIds'] = cartItemsIds

        return HttpResponse(request.session['cartItems'])

def cartItems(request):
    if request.is_ajax():
        items = request.session['cartItems']
        return JsonResponse(items, safe=False)

def index(request):

    # with open('static/data.json', 'w') as outfile:
    #     json.dump(data, outfile)
    print(BASE_DIR)

    decoded_json = json.loads(open(BASE_DIR+'/static/data.json').read())
    # print(decoded_json)

    return render(request, 'index.html', {'data': decoded_json})

def items_filter(request):

    if request.is_ajax():

        if 'date' in request.GET:
            date = request.GET.get('date', '')
            # convert it
            # date = dt.strptime(date, '%m/%d/%Y')
            date_type = request.GET.get('date_type', False)
            request.session['filterQuery'][date_type] = date

        if 'inStock' in request.GET:
            if request.GET.get('inStock', False) == 'true':
                request.session['filterQuery']['in_stock'] = True
                request.session.modified = True
            else:
                request.session['filterQuery']['in_stock'] = False

            # items = filter(lambda item: item['in_stock'], items)

        if 'price_from' in request.GET:

            price_from = request.GET.get('price_from', 0)

            if price_from is not None and len(price_from) > 0 :

                price_from = float(price_from.replace(',', '.'))

            else:
                price_from = 0

            request.session['filterQuery']['price_from'] = price_from
            request.session.modified = True

        if 'price_to' in request.GET:

            price_to = request.GET.get('price_to', 0)

            if price_to is not None and len(price_to) > 0 :

                price_to = float(price_to.replace(',', '.'))

            else:
                price_to = 0

            request.session['filterQuery']['price_to'] = price_to
            request.session.modified = True

        if 'color' in request.GET:

            color = request.GET.get('color', 'None')

            request.session['filterQuery']['color'] = color
            request.session.modified = True

        filterQuery = request.session['filterQuery']

        # sss = filter(lambda item:
        #        # dt.strptime(item['issue_date'], '%m/%d/%Y') <= dt.strptime(filterQuery['date_to'], '%m/%d/%Y')
        #        dt.strptime(item['issue_date'], '%m/%d/%Y') <= dt.strptime(filterQuery['date_to'], '%m/%d/%Y')
        #        , data
        #              )
        # sss = list(sss)

        print(filterQuery)

        items = filter(
            lambda item:
            float(item['price'].replace(',', '.')) >= filterQuery['price_from']
            and
            float(item['price'].replace(',', '.')) <= filterQuery['price_to']
            and
            dt.strptime(item['issue_date'], '%m/%d/%Y') >= dt.strptime(filterQuery['date_from'], '%m/%d/%Y')
            and
            dt.strptime(item['issue_date'], '%m/%d/%Y') <= dt.strptime(filterQuery['date_to'], '%m/%d/%Y')
            , data
        )
        if filterQuery['in_stock'] == True:
            items = filter(lambda item: item['in_stock'] == True, items)

        if filterQuery['color'] != 'None':
            items = filter(lambda item: item['color'] == filterQuery['color'], items)

        items = list(items)
        print(items)

        context = {
            'items': items,
        }

        response = render(request, 'filter_results.html', context)

    # return render(request, 'filter_results.html', context)
    return HttpResponse(response)

def filter_reset(request):
    if 'filterQuery' in request.session:
        del request.session['filterQuery']

    request.session['filterQuery'] = {
        "name": "",
        "color": "None",
        "date_from": "01/01/1990",
        "date_to": "01/01/2100",
        "in_stock": False,
        "price_from": 0,
        "price_to": float("inf"),
    }
    request.session.modified = True
    return HttpResponse('reset 200')

@anonymous_required
def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            auth_login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})

