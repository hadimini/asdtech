from .views import data
def cartItems(request):
    if not request.session.get('cartItems'):
        request.session['cartItems'] = []

    if not request.session.get('cartItemsIds'):
        request.session['cartItemsIds'] = []

    if len(request.session['cartItems']) > 0:

        cartItemsIds = request.session['cartItemsIds']
        for item in request.session['cartItems']:
            cartItemsIds.append(item['id'])
        request.session['cartItemsIds'] = cartItemsIds


    def_items = [ # just for reset purpose

        # {
        #     "id": 1,
        #     "name": "Book",
        #     "color": "Red",
        #     "issue_date": "10/04/2018",
        #     "in_stock": True,
        #     "price": "20,5",
        #     "image": "https://i.pinimg.com/236x/98/a5/1f/98a51f3e27a962bf86951645d7a9bd9d--sales-quotes-product-launch.jpg"
        # },

    ]

    # request.session['cartItems'] = def_items
    # request.session['cartItemsIds'] = []

    return {
        'cartItemsSession': request.session['cartItems'],
        'cartItemsLen': len(request.session['cartItems']),
        'cartItemsIds': request.session['cartItemsIds']
    }