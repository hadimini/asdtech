"""
WSGI config for config project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
import sys
import site

site.addsitedir('/var/www/asdtech/venv/lib/python3.5/site-packages/')

os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings'

sys.path.append('/var/www')
sys.path.append('/var/www/asdtech')
sys.path.append('/var/www/asdtech/django')

activate_env=os.path.expanduser('/var/www/asdtech/venv/bin/activate_this.py')
exec(open(activate_env).read(), dict(__file__=activate_env))


from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')

application = get_wsgi_application()
